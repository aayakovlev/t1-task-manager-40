package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.ProjectService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ProjectServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private static final ProjectRepository repository = sqlSession.getMapper(ProjectRepository.class);

    @NotNull
    private static final ProjectService service = new ProjectServiceImpl(connectionService);

    @Before
    public void init() throws EntityEmptyException {
        service.save(PROJECT_USER_ONE);
        service.save(PROJECT_USER_TWO);
    }

    @After
    public void after() {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnProject() {
        @Nullable final Project project = repository.findById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_USER_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_USER_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_USER_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_USER_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_USER_ONE.getCreated(), project.getCreated());
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnNull() {
        @Nullable final Project project = repository.findById(USER_ID_NOT_EXISTED ,PROJECT_ID_NOT_EXISTED);
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullProject_Expect_ReturnProject() {
        repository.save(PROJECT_ADMIN_ONE);
        sqlSession.commit();
        @Nullable final Project project = repository.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_ADMIN_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getCreated(), project.getCreated());
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListProjects() {
        final List<Project> projects = repository.findAllUserId(COMMON_USER_ONE.getId());
        for (int i = 0; i < projects.size(); i++) {
            Assert.assertEquals(USER_PROJECT_LIST.get(i).getName(), projects.get(i).getName());
        }
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedProjectList() {
        final List<Project> projects = repository.findAllSorted(COMMON_USER_ONE.getId(), "name");
        for (int i = 0; i < projects.size(); i++) {
            Assert.assertEquals(USER_PROJECT_SORTED_LIST.get(i).getName(), projects.get(i).getName());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedProject_Expect_ReturnProject() {
        repository.save(PROJECT_ADMIN_TWO);
        sqlSession.commit();
        repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
        sqlSession.commit();
    }

    @Test
    public void When_RemoveNotProject_Expect_ThrowsEntityNotFoundException() {
        repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId());
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_ZeroCountProjects() {
        repository.save(PROJECT_ADMIN_ONE);
        sqlSession.commit();
        repository.save(PROJECT_ADMIN_TWO);
        sqlSession.commit();
        repository.clear(ADMIN_USER_ONE.getId());
        sqlSession.commit();
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedProject_Expect_Project() {
        repository.save(PROJECT_ADMIN_TWO);
        sqlSession.commit();
        repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
        sqlSession.commit();
    }

    @Test
    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId())
        );
    }

}
