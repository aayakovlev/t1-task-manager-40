package ru.t1.aayakovlev.tm.service;

public interface BackupService {

    boolean getBackupEnabled();

}
