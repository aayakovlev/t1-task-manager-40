package ru.t1.aayakovlev.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface TaskRepository {

    @Delete("DELETE FROM task_manager.tm_task WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") final String userId);

    @Delete("DELETE FROM task_manager.tm_task")
    void clearAll();

    @Select("SELECT COUNT(id) FROM task_manager.tm_task WHERE user_id = #{userId}")
    int count(@NotNull @Param("userId") final String userId);

    @Nullable
    @Select("SELECT * FROM task_manager.tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAll();

    @Nullable
    @Select("SELECT * FROM task_manager.tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    );

    @Nullable
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_manager.tm_task WHERE user_id = #{user_id} ORDER BY #{order}")
    List<Task> findAllSorted(@NotNull @Param("userId") final String userId, @NotNull @Param("order") final String order);

    @Nullable
    @Select("SELECT * FROM task_manager.tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Select("SELECT * FROM task_manager.tm_task WHERE id = #{id} AND user_id = #{userId}")
    Task findById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Delete("DELETE FROM task_manager.tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Insert("INSERT INTO task_manager.tm_task (id, created, name, description, status, user_id, project_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})"
    )
    void save(@NotNull final Task task);

    @Update("UPDATE task_manager.tm_task SET " +
            "name = #{name}, description = #{description}, status = #{status}, created = #{created}, " +
            "user_id = #{userId}, project_id = #{project_id} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull final Task task);

}
