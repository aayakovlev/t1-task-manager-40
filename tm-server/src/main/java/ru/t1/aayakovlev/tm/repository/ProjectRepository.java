package ru.t1.aayakovlev.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.List;

public interface ProjectRepository {

    @Delete("DELETE FROM task_manager.tm_project WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") final String userId);

    @Delete("DELETE FROM task_manager.tm_project")
    void clearAll();

    @Select("SELECT COUNT(id) FROM task_manager.tm_project WHERE user_id = #{userId}")
    int count(@NotNull @Param("userId") final String userId);

    @Nullable
    @Select("SELECT * FROM task_manager.tm_project")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAll();

    @Nullable
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Select("SELECT * FROM task_manager.tm_project WHERE user_id = #{user_id} ORDER BY #{order}")
    List<Project> findAllSorted(@NotNull @Param("userId") final String userId, @NotNull @Param("order") final String order);

    @Nullable
    @Select("SELECT * FROM task_manager.tm_project WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAllUserId(@NotNull @Param("userId") final String userId);

    @Nullable
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Select("SELECT * FROM task_manager.tm_project WHERE id = #{id} AND user_id = #{userId}")
    Project findById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Delete("DELETE FROM task_manager.tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String id);

    @Insert("INSERT INTO task_manager.tm_project (id, created, name, description, status, user_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})"
    )
    void save(@NotNull final Project project);

    @Update("UPDATE task_manager.tm_project SET " +
            "name = #{name}, description = #{description}, status = #{status}, created = #{created}, " +
            "user_id = #{userId} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull final Project project);

}
