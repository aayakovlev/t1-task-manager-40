package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.UserEmailExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserLoginExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.exception.field.PasswordEmptyException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.UserRepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.UserService;
import ru.t1.aayakovlev.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserServiceImpl implements UserService {

    @NotNull
    private final ConnectionService connectionService;

    @NotNull
    private final PropertyService propertyService;

    public UserServiceImpl(
            @NotNull final PropertyService propertyService,
            @NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    private UserRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(UserRepository.class);
    }

    @NotNull
    private ProjectRepository getProjectRepository(@NotNull final SqlSession session) {
        return session.getMapper(ProjectRepository.class);
    }

    @NotNull
    private TaskRepository getTaskRepository(@NotNull final SqlSession session) {
        return session.getMapper(TaskRepository.class);
    }

    @Override
    public void clear() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final UserRepository repository = getRepository(session);
            repository.clear();
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new UserEmailExistsException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return save(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return save(user);
    }

    @NotNull
    public User save(@NotNull final User user) {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull UserRepository repository = session.getMapper(UserRepository.class);
            repository.save(user);
            session.commit();
            return user;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final UserRepository repository = getRepository(session);
            @Nullable final User user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final UserRepository repository = getRepository(session);
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final UserRepository repository = getRepository(session);
            return repository.findByEmail(email) != null;
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final UserRepository repository = getRepository(session);
            return repository.findByLogin(login) != null;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final User user = findByLogin(login);
            user.setLocked(true);
            @NotNull final UserRepository repository = getRepository(session);
            repository.update(user);
            session.commit();
            return user;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId) throws AbstractException {
        if (userId == null) throw new IdEmptyException();
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final UserRepository repository = getRepository(session);
            repository.removeById(userId);
            @NotNull final TaskRepository taskRepository = getTaskRepository(session);
            taskRepository.clear(userId);
            @NotNull final ProjectRepository projectRepository = getProjectRepository(session);
            projectRepository.clear(userId);
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        removeById(user.getId());
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final User user = findByEmail(email);
        removeById(user.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final UserRepository repository = getRepository(session);
            repository.update(user);
            session.commit();
            return user;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final UserRepository repository = getRepository(session);
            repository.update(user);
            session.commit();
            return user;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final UserRepository repository = getRepository(session);
            repository.update(user);
            session.commit();
            return user;
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public User findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final UserRepository repository = getRepository(session);
            @Nullable final User user = repository.findById(id);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    public List<User> findAll() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final UserRepository repository = getRepository(session);
            @Nullable final List<User> users = repository.findAll();
            if (users == null) return new ArrayList<>();
            return users;
        }
    }

    @Override
    public void set(@Nullable final List<User> users) {
        if (users == null || users.size() == 0) return;
        for (@NotNull final User user: users) {
            save(user);
        }
    }

}
