package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface TaskService {

    void clear();

    void clear(@Nullable final String userId) throws UserIdEmptyException;

    int count(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException;

    @NotNull
    List<Task> findAll();

    @NotNull
    Task findById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    Task save(@NotNull final Task model);

    @NotNull
    Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException;

    @NotNull
    Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException;

    @NotNull
    Task create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException;

    @NotNull
    List<Task> findByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractFieldException;

    @NotNull
    Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException;

    void set(@Nullable final List<Task> tasks);

}
