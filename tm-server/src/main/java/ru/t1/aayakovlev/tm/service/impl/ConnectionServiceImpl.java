package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.mybatis.caches.hazelcast.LoggingHazelcastCache;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.*;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import javax.sql.DataSource;

public final class ConnectionServiceImpl implements ConnectionService {

    // TODO: 28.11.2022 добавить работу с h2 для тестов

    @NotNull
    private final DatabaseProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionServiceImpl(@NotNull final DatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
        initDatasource();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @SneakyThrows
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseURL();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(DataRepository.class);
        configuration.addMapper(ProjectRepository.class);
        configuration.addMapper(TaskRepository.class);
        configuration.addMapper(UserRepository.class);
        configuration.addMapper(SessionRepository.class);
        configuration.setCacheEnabled(true);
        configuration.addCache(new LoggingHazelcastCache("tm"));
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    private void initDatasource() {
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final DataRepository dataRepository = session.getMapper(DataRepository.class);
        dataRepository.initSchema();
        dataRepository.initUserTable();
        dataRepository.initSessionTable();
        dataRepository.initProjectTable();
        dataRepository.initTaskTable();
        @NotNull final UserRepository userRepository = session.getMapper(UserRepository.class);
        if (userRepository.findByLogin("admin") == null) {
            userRepository.save(new User("admin", Role.ADMIN, "5759bb213e44ab5d7527cb0c29ffb911"));
            System.out.println("Admin user with password `admin` initialised");
        }
        session.commit();
    }

}
