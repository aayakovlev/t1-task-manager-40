package ru.t1.aayakovlev.tm.repository;

import org.apache.ibatis.annotations.Update;

public interface DataRepository {

    @Update("CREATE SCHEMA IF NOT EXISTS task_manager")
    void initSchema();

    @Update("CREATE TABLE IF NOT EXISTS task_manager.tm_user" +
            "(" +
            "    id varchar(36) NOT NULL," +
            "    login varchar(64) NOT NULL," +
            "    password_hash varchar(32) NOT NULL," +
            "    email varchar(64)," +
            "    role varchar(64)," +
            "    first_name varchar(64)," +
            "    last_name varchar(64)," +
            "    middle_name varchar(64)," +
            "    locked boolean," +
            "    CONSTRAINT user_pkey PRIMARY KEY (id)" +
            ");")
    void initUserTable();

    @Update("CREATE TABLE IF NOT EXISTS task_manager.tm_session" +
            "(" +
            "    id varchar(36) NOT NULL," +
            "    date timestamp NOT NULL," +
            "    role varchar(64)," +
            "    user_id varchar(36) NOT NULL," +
            "    CONSTRAINT session_pkey PRIMARY KEY (id)" +
            ");")
    void initSessionTable();

    @Update("CREATE TABLE IF NOT EXISTS task_manager.tm_project" +
            "(" +
            "    id varchar(36) NOT NULL," +
            "    name varchar(64) NOT NULL," +
            "    description varchar(255)," +
            "    user_id varchar(36)," +
            "    status varchar(64)," +
            "    created timestamp NOT NULL," +
            "    CONSTRAINT project_pkey PRIMARY KEY (id)" +
            ");")
    void initProjectTable();

    @Update("CREATE TABLE IF NOT EXISTS task_manager.tm_task" +
            "(" +
            "    id varchar(36) NOT NULL," +
            "    name varchar(64) NOT NULL," +
            "    description varchar(255)," +
            "    user_id varchar(36)," +
            "    project_id varchar(36)," +
            "    status varchar(64) NOT NULL," +
            "    created timestamp NOT NULL," +
            "    CONSTRAINT task_pkey PRIMARY KEY (id)" +
            ");")
    void initTaskTable();

}
