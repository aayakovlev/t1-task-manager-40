package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private Task task;

    @Nullable
    private List<Task> tasks;

    public AbstractTaskResponse(@Nullable final Task task) {
        this.task = task;
    }

    public AbstractTaskResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}
