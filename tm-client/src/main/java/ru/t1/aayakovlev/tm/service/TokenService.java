package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface TokenService {

    @NotNull
    String getToken();

    void setToken(final String token);

}
